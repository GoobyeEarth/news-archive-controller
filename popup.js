

const DATE_PERIOD = 0;
const DATE_COUNT = 1;
const QUERYS_NUM = 20;
var querys = {
    query0 : "",
    query1 : "",
    query2 : "",
    query3 : "",
    query4 : "",
    query5 : "",
    query6 : "",
    query7 : "",
    query8 : "",
    query9 : "",
    query10 : "",
    query11 : "",
    query12 : "",
    query13 : "",
    query14 : "",
    query15 : "",
    query16 : "",
    query17 : "",
    query18 : "",
    query19 : ""
  };

loadStrage();

$(function() {
  
  $('#date').change(function() {
     
  });

  $('#forward').click(function(){
    countDate(DATE_COUNT);
    loadUrl();
    saveStrage();
  });

  $('#back').click(function(){
    countDate(- DATE_COUNT);
    loadUrl();
    saveStrage();
    
  });

});

function loadStrage(){

  var defaultData = {
    date : "2015/12/12"
  };

  chrome.storage.sync.get(defaultData,
  function(items) {
    $('#date').val(items.date);
  });

  chrome.storage.sync.get(querys,
  function(items) {
    querys.query0=items.query0;
    querys.query1=items.query1;
    querys.query2=items.query2;
    querys.query3=items.query3;
    querys.query4=items.query4;
    querys.query5=items.query5;
    querys.query6=items.query6;
    querys.query7=items.query7;
    querys.query8=items.query8;
    querys.query9=items.query9;
    querys.query10=items.query10;
    querys.query11=items.query11;
    querys.query12=items.query12;
    querys.query13=items.query13;
    querys.query14=items.query14;
    querys.query15=items.query15;
    querys.query16=items.query16;
    querys.query17=items.query17;
    querys.query18=items.query18;
    querys.query19=items.query19;
    
  });
}

function saveStrage(){
  var data = {
    date : $('#date').val()
  };
  chrome.storage.sync.set(data, function(){});

}



function loadUrl(){
   var date = new Date($('#date').val());
    var start = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    date.setDate(date.getDate() + DATE_PERIOD);
    var end = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();

    chrome.tabs.query({}, function(tabs) {
      if(querys.query0 != "" && 0 < tabs.length) chrome.tabs.update(tabs[0].id, {url: getUrl(querys.query0, start, end)});
      if(querys.query1 != "" && 1 < tabs.length) chrome.tabs.update(tabs[1].id, {url: getUrl(querys.query1, start, end)});
      if(querys.query2 != "" && 2 < tabs.length) chrome.tabs.update(tabs[2].id, {url: getUrl(querys.query2, start, end)});
      if(querys.query3 != "" && 3 < tabs.length) chrome.tabs.update(tabs[3].id, {url: getUrl(querys.query3, start, end)});
      if(querys.query4 != "" && 4 < tabs.length) chrome.tabs.update(tabs[4].id, {url: getUrl(querys.query4, start, end)});
      if(querys.query5 != "" && 5 < tabs.length) chrome.tabs.update(tabs[5].id, {url: getUrl(querys.query5, start, end)});
      if(querys.query6 != "" && 6 < tabs.length) chrome.tabs.update(tabs[6].id, {url: getUrl(querys.query6, start, end)});
      if(querys.query7 != "" && 7 < tabs.length) chrome.tabs.update(tabs[7].id, {url: getUrl(querys.query7, start, end)});
      if(querys.query8 != "" && 8 < tabs.length) chrome.tabs.update(tabs[8].id, {url: getUrl(querys.query8, start, end)});
      if(querys.query9 != "" && 9 < tabs.length) chrome.tabs.update(tabs[9].id, {url: getUrl(querys.query9, start, end)});
      if(querys.query10 != "" && 10 < tabs.length) chrome.tabs.update(tabs[10].id, {url: getUrl(querys.query10, start, end)});
      if(querys.query11 != "" && 11 < tabs.length) chrome.tabs.update(tabs[11].id, {url: getUrl(querys.query11, start, end)});
      if(querys.query12 != "" && 12 < tabs.length) chrome.tabs.update(tabs[12].id, {url: getUrl(querys.query12, start, end)});
      if(querys.query13 != "" && 13 < tabs.length) chrome.tabs.update(tabs[13].id, {url: getUrl(querys.query13, start, end)});
      if(querys.query14 != "" && 14 < tabs.length) chrome.tabs.update(tabs[14].id, {url: getUrl(querys.query14, start, end)});
      if(querys.query15 != "" && 15 < tabs.length) chrome.tabs.update(tabs[15].id, {url: getUrl(querys.query15, start, end)});
      if(querys.query16 != "" && 16 < tabs.length) chrome.tabs.update(tabs[16].id, {url: getUrl(querys.query16, start, end)});
      if(querys.query17 != "" && 17 < tabs.length) chrome.tabs.update(tabs[17].id, {url: getUrl(querys.query17, start, end)});
      if(querys.query18 != "" && 18 < tabs.length) chrome.tabs.update(tabs[18].id, {url: getUrl(querys.query18, start, end)});
      if(querys.query19 != "" && 19 < tabs.length) chrome.tabs.update(tabs[19].id, {url: getUrl(querys.query19, start, end)});
      // chrome.tabs.update(tabs[0].id, {url: getUrl(querys.query0, start, end)});
    });
}



function countDate(count){
    var date = new Date($('#date').val());
    date.setDate(date.getDate() + count);
    var nextDateStr = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
    $('#date').val(nextDateStr);

}

function getUrl(query, startDate, endDate){
  var url = "https://www.google.com/search?as_q=";
  url += query;
  url +=  "&as_qdr=all&as_occt=any&safe=off&tbm=nws&ie=UTF-8&oe=UTF-8&tbs=lr:lang_1ja,cdr:1,cd_min:";
  url += startDate + ",cd_max:";
  url += endDate;
  return url;
}





